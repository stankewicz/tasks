<?php declare(strict_types=1);
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Task Model
 *
 * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
 * @since 1.0.0
 * @version 1.0.0
 */
class Task extends Model {
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'status'
    ];

    public function setTitleAttribute($value) {
        if(!$value) {
            throw new \InvalidArgumentException('Title is required');
        }

        $this->attributes['title'] = filter_var($value, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    }

    public function setDescriptionAttribute($value) {
        if(!$value) {
            throw new \InvalidArgumentException('Description is required');
        }
        $this->attributes['description'] = filter_var($value, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    }
}
