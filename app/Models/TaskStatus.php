<?php declare(strict_types=1);
namespace App\Models;

/**
 * Represents a status of Task
 *
 * @author Adriano Stankewicz
 * @since 1.0.0
 * @version 1.0.0
 */
enum TaskStatus: int
{
   case Pending = 1;
   case Completed = 2;

   /**
    * Status in text
    *
    * @return string
    * @author Adriano Stankewicz
    * @since 1.0.0
    * @version 1.0.0
    */
   public function status(): string
   {
      return match($this) {
        TaskStatus::Pending => 'pending',
        TaskStatus::Completed => 'completed',
      };
   }
}
