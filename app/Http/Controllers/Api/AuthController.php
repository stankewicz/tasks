<?php declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Authentication controller for users api
 * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
 * @version 1.0.0
 * @since 1.0.0
 */
class AuthController extends Controller {

    /**
     * Authenticate a user in app
     *
     * @param Request $request
     * @return JsonResponse
     * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
     * @version 1.0.0
     * @since 1.0.0
     */
    public function login(Request $request): JsonResponse {
        if(in_array('', $request->only('email', 'password'))){
            return response()->json(['error' => __('auth.failed')], 401);
        }

        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return response()->json(['error' => __('auth.failed')], 401);
        }

        $credentials = [
            'email' => filter_var($request->email, FILTER_SANITIZE_EMAIL),
            'password' => filter_var($request->password, FILTER_SANITIZE_FULL_SPECIAL_CHARS)
        ];

        if(!Auth::attempt($credentials)){
            return response()->json(['error' => __('auth.failed')], 401);
        }

        return response()->json([
            'message' => __('auth.success'),
            'token'   => Auth::user()->generateToken(),
        ], 200);
    }

    /**
     * Deauthenticate a user in app
     *
     * @return JsonResponse
     * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
     * @version 1.0.0
     * @since 1.0.0
     */
    public function logout(): JsonResponse {
        if(Auth::check()){
            Auth::user()->tokens()->delete();
        }
        return response()->json(['message' => __('auth.logout')]);
    }
}
