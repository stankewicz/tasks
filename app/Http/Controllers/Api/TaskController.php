<?php declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\TaskStatus;

/**
 * Task controller
 * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
 * @version 1.0.0
 * @since 1.0.0
 */
class TaskController extends Controller {

    /**
     * Display a listing of the tasks.
     *
     * @return JsonResponse
     * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
     * @version 1.0.0
     * @since 1.0.0
     */
    public function index(): JsonResponse {
        $tasks = Task::all();
        return response()->json(['task' => $tasks], 200);
    }

    /**
     * Store a newly created task in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
     * @version 1.0.0
     * @since 1.0.0
     */
    public function store(Request $request): JsonResponse {
        if(!$request->title){
            return response()->json(['messge' => __('task.required.title')], 400);
        }

        if(!$request->description){
            return response()->json(['messge' => __('task.required.description')], 400);
        }

        $task = Task::create([
            'title' => filter_var($request->title, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
            'description' => filter_var($request->description, FILTER_SANITIZE_FULL_SPECIAL_CHARS),
            'status' => TaskStatus::from(filter_var($request->status, FILTER_VALIDATE_INT))
        ]);

        if(!$task) {
            return response()->json(['messge' => __('task.actions.create_failed')], 500);
        }

        return response()->json(['message' => __('task.actions.create_success')], 201);
    }

    /**
     * Display the specified task.
     *
     * @param int $id
     * @return JsonResponse
     * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
     * @version 1.0.0
     * @since 1.0.0
     */
    public function show(int $id): JsonResponse {
        if(!filter_var($id, FILTER_VALIDATE_INT)){
            return response()->json(['messge' => __('task.required.id')]);
        }

        $task = Task::where('id', $id)->first();

        if(!$task){
            return response()->json(['messge' => __('task.message.not_found')], 400);
        }

        return response()->json(['task' => $task], 200);
    }

    /**
     * Update the specified task in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
     * @version 1.0.0
     * @since 1.0.0
     */
    public function update(Request $request, int $id): JsonResponse {
        if(!filter_var($id, FILTER_VALIDATE_INT)){
            return response()->json(['messge' => __('task.required.id')]);
        }

        $task = Task::where('id', $id)->first();

        if(!$task){
            return response()->json(['messge' => __('task.message.not_found')], 400);
        }

        if(!$request->title){
            return response()->json(['messge' => __('task.required.title')], 400);
        }

        if(!$request->description){
            return response()->json(['messge' => __('task.required.description')], 400);
        }

        $task->title = $request->title;
        $task->description = $request->description;
        $task->status = $request->status || $task->status;

        $isSaved = $task->save();

        if(!$isSaved) {
            return response()->json(['messge' => __('task.actions.update_failed')], 500);
        }

        return response()->json(['message' => __('task.actions.update_success')], 200);
    }

    /**
     * Remove the specified task from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
