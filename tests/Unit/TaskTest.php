<?php declare(strict_types=1);
namespace Tests\Unit;

use App\Models\Task;
use App\Models\TaskStatus;
use PHPUnit\Framework\TestCase;

/**
 * Unit tests for Task
 */
class TaskTest extends TestCase {

    /**
     * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
     * @since 1.0.0
     * @version 1.0.0
     * @test
     */
    public function shouldNotCreateATask(): void {
        $this->expectException(\InvalidArgumentException::class);
        $task = new Task();
        $task->fill([
            'title' => 'Title 01',
            'description' => '',
            'status' => TaskStatus::Pending->value,
        ]);
    }

    /**
     * @author Adriano Stankewicz <adriano.stankewicz@gmail.com>
     * @since 1.0.0
     * @version 1.0.0
     * @test
     */
    public function shouldCreateATask(): void {
        $task = new Task();
        $task->fill([
            'title' => 'Title 01',
            'description' => 'Description 01',
            'status' => TaskStatus::Pending->value,
        ]);

        $this->assertNotNull($task);
    }
}
