<?php declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//Errors
Route::get('/404', function(){
    return response()->json(['error' => 'Página não encontrada.'], 404);
})->name('api.error.404');

Route::get('/401', function(){
    return response()->json(['error' => 'Não possui acesso ao recurso solicitado.'], 401);
})->name('api.error.401');

//Retrieve a status of api
Route::get('/status', function(){
    return response()->json(['status' => 'Online'], 200);
})->name('api.status');

//Retrieve a token with user
Route::post('/login', [AuthController::class, 'login'])->name('api.login');

Route::middleware('auth:sanctum')->group(function(){

    //Auth
    Route::post('/logout', [AuthController::class, 'logout'])->name('api.logout');

    //Tasks
    Route::post('/task/create', [TaskController::class, 'store'])->name('api.task.store');
    Route::get('/task/{id}', [TaskController::class, 'show'])->name('api.task.show');
    Route::put('/task/edit/{id}', [TaskController::class, 'update'])->name('api.task.edit');
    Route::delete('/task/remove', [TaskController::class, 'remove'])->name('api.task.remove');
    Route::get('/tasks', [TaskController::class, 'index'])->name('api.tasks');
});
