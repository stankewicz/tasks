### Tasks

Este é um projeto de api para um gerenciador de tarefas, com ele é possível criar,
editar e visualizar tarefas.

Pré-requisitos:
- MySQL 5.6
- PHP 8.0 ou superior
- Composer 2.0

Para executar o projeto em sua máquina você deve seguir as seguintes instruções.

Faça um checkout do projeto.

Atualize as dependências com o comando:

- composer update

Crie um arquivo .env com base no .env.example e atualize as configurações do banco de dados.

Crie a estrutura do banco de dados com o comando:

- php artisan migrate

E por fim, inicie o servidor local com o comando:

- php artisan serve

A url default, nesse caso localmente, para o acesso a API é:

- http://localhost:8000/api

A documentação da API está disponível no link abaixo:

- https://documenter.getpostman.com/view/4848755/2s9XxztCQB

Ou na raiz do projeto, pasta _docs.
