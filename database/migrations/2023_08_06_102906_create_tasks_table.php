<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title', 50);
            $table->string('description', 191);
            $table->tinyInteger('status')->unsigned()->default(1)->comment('1-Pending | 2-Completed');
            $table->timestamp('created_at')->useCurrent()->comment('Audit');
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate()->comment('Audit');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
