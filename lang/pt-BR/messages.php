<?php

return [
    'active' => 'ativo',
    'inactive' => 'inativo',
    'registered' => 'registrado',
    'canceled' => 'cancelado',
    'open' => 'aberto',
    'closed' => 'fechado',
    'finished' => 'finalizado',
    'confirmed' => 'confirmado'
];
