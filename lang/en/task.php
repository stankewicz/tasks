<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'required' => [
        'id'          => 'Id is required',
        'title'       => 'Title is required',
        'description' => 'Description is required',
    ],

    'actions' => [
        'create_success' => 'Task created with success',
        'create_failed' => 'Occurred an error to create a task',
        'update_success' => 'Task updated with success',
        'update_failed' => 'Occurred an error to update a task'
    ],

    'message' => [
        'not_found' => 'Task not found'
    ]

];
